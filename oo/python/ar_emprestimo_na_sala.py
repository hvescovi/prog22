class Livro:
    def __init__(self, nome, isbn):
        self.nome = nome
        self.isbn = isbn

livro1 = Livro("Python 3", "39483857374")
print("Livro:", livro1.nome,  ", ", livro1.isbn)

class Pessoa:
    def __init__(self, nome="", email="", telefone=""):
        self.nome = nome
        self.telefone = telefone
        self.email = email
    def __str__(self):
        return f'O nome desta pessoa é {self.nome}, telefone = {self.telefone}, email = {self.email}'
        
        '''
        s = f'O nome desta pessoa é {self.nome}, telefone = {self.telefone}'
        # tem email definido?
        if self.email != "":
            # então, adiciona email no retorno __str__ :-)
            s += f', email = {self.email}'
        else:
            s += " ((esta pessoa não tem email))"
        return s
        '''

pessoa1 = Pessoa(nome="João", telefone="91232-0123")        
print("Pessoa:", pessoa1)

class ExemplarDeLivro:
    def __init__(self, livro=None, codigo=""):
        self.livro = livro
        self.codigo = codigo
    def __str__(self):
        return f'{self.livro.nome}, {self.livro.isbn}, {self.codigo}'

exemplar1 = ExemplarDeLivro(codigo="4uyr34uy343", livro=livro1)
print(exemplar1)

class Aluno(Pessoa):
    def __init__(self, nome="", email="", telefone="", matricula=""):
        self.matricula = matricula
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self):
        # return f'{self.nome}, {self.email}, {self.telefone}, {self.matricula}'
        return f'{super().__str__()}, {self.matricula}'
       

aluno1 = Aluno(nome = "Claudecir", email="claudi@gmail.com", 
        telefone="9 12341234", matricula = "12345")
print(aluno1)

'''
    def __init__(self, matricula=""):
        self.matricula=matricula
    
aluno1 = Aluno(matricula="12345")
aluno1.nome = "Claudecir"
aluno1.email = "claudi@gmail.com"
aluno1.telefone = "9 1232 1231"
print(aluno1.nome)
'''




class Servidor(Pessoa):
    def __init__(self, nome="", email="", telefone="", siape=""):
        self.siape = siape
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self):
        # return f'{self.nome}, {self.email}, {self.telefone}, {self.matricula}'
        return f'{super().__str__()}, {self.siape}'
       

servidor1 = Servidor(nome = "Josisneclay", email="josys@gmail.com", 
        telefone="9 987678876", siape = "4837583")
print(servidor1)


class Emprestimo:
    def __init__(self, emprestado=None, devolvido=None, 
                        pessoa=None, exemplar=None):
        self.emprestado = emprestado
        self.devolvido = devolvido
        self.pessoa = pessoa
        self.exemplar = exemplar
    def __str__(self):
        return f'{self.emprestado}, {self.devolvido}, {self.pessoa}, {self.exemplar}'

from datetime import *

emprestimo1 = Emprestimo(emprestado = datetime.Date(2022, 10, 1),   #datetime.today(), 
            pessoa=aluno1, exemplar=exemplar1)
print("Emprestimo:", emprestimo1)

class Multa:
    def __init__(self, emprestimo=None, valor=0.0, paga=False):
        self.emprestimo = emprestimo
        self.valor = valor
        self.paga = paga
    def __str__(self):
        return f'{self.emprestimo}, {self.valor}, {self.paga}'

# teste do procedimento de devolução

# atribui a data da devolução
emprestimo1.devolvido = datetime.Date(2022, 11, 30)
# se o emprestimo foi feito para aluno
if isinstance(emprestimo1.pessoa, Aluno):
  # o prazo será 15 dias
  prazo = 15
# senão
else:
  # o prazo será 30 dias
  prazo = 30

# calcula quantos dias se passaram desde o empréstimo até a data da devolução
dias = abs(emprestimo1.devolvido - emprestimo1.emprestado).days
# se a quantidade de dias não estiver no prazo
if dias > prazo:
  # gera a multa, dois reais por dia
  multa1 = Multa(emprestimo = emprestimo1, valor=dias*2)
  print("Multa", multa1)