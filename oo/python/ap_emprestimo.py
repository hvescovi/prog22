class Livro:
    def __init__(self, nome="", ISBN=""):
        self.nome = nome
        self.isbn = ISBN
    def __str__(self):
        return f'{self.nome}, {self.isbn}'

class ExemplarDeLivro:
    def __init__(self, livro, codigo):
        self.livro = livro
        self.codigo = codigo
    def __str__(self):
        return f'{self.livro}, {self.codigo}'

# teste
livro1 = Livro("Python 3", "123321123")
ex1 = ExemplarDeLivro(livro1, "AX123")
print("exemplar de livro:", ex1)

class Pessoa:
    def __init__(self, nome="", email="", telefone=""):
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self):
        return f'{self.nome}, '+\
               f'{self.email}, {self.telefone}'

class Aluno(Pessoa):
    def __init__(self, nome="", email="", tel="", matricula=""):
        super().__init__(nome=nome, email=email, telefone=tel)
        self.matricula = matricula
    def __str__(self):
        return f'{super().__str__()}, {self.matricula}'

# teste do aluno
aluno1 = Aluno(nome="Joaozinho", email="josilva@gmail.com", tel = "91122-3344", matricula = "2022010203")
print("aluno:", aluno1)

class Servidor(Pessoa):
    def __init__(self, nome="", email="", tel="", siape=""):
        super().__init__(nome=nome, email=email, telefone=tel)
        self.siape = siape
    def __str__(self):
        return f'{super().__str__()}, {self.siape}'

serv1 = Servidor(nome="John Them", email="jothen@gmail.com", tel="9 1221-2332", siape="1928374")
print("servidor:", serv1)

class Emprestimo():
    def __init__(self, emprestado=None, devolvido=None, pessoa=None, exemplar=None):
        self.emprestado = emprestado
        self.devolvido = devolvido
        self.pessoa = pessoa
        self.exemplar = exemplar
    def __str__(self):
        return f'{self.emprestado:%d/%m/%Y}, {self.devolvido}, {self.pessoa}, {self.exemplar}'

from datetime import *

# teste do emprestimo
emp1 = Emprestimo(emprestado = datetime.now(), pessoa = aluno1, exemplar = ex1)
print("Empréstimo:", emp1)