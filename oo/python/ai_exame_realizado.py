from ac_classe_pessoa import *

class Exame:
    def __init__(self, descricao, faixa):
        self.descricao = descricao
        self.faixa = faixa

    def __str__(self):
        return f'Descrição: {self.descricao}, faixa: {self.faixa}'

class ExameRealizado:
    # def __init__(self, data, resultado, pessoa, exame):
    # er = ExameRealizado("14/01/2020", "219/ng/mL", p1, e1)        
    def __init__(self, data="", resultado="", pessoa=None, exame=None):
    # er = ExameRealizado(data = "14/01/2020", resultado="219/ng/mL", exame=e1)
        self.data = data
        self.resultado = resultado
        self.pessoa = pessoa
        self.exame = exame

    # método que expressa o objeto em formato textual
    def __str__(self):
        return f'Exame realizado em {self.data}, resultado = {self.resultado}, '+\
            f'Paciente: {str(self.pessoa)}, Exame: {str(self.exame)}'

# cria uma pessoa
p1 = Pessoa(nome = "João da Silva", email = "josilva@gmail.com",
            telefone = "47 99012 3232")

# cria um exame de João
e1 = Exame("Vitamina B12", "entre 100 e 200 ng/mL")

# cria um exame realizado
er1 = ExameRealizado(data="14/01/2020",
        resultado="219ng/mL", pessoa=p1, exame=e1)

# mostra tudo
print(er1)

# como faço para mostrar o nome da pessoa que teve o exame realizado (er1)
print(er1.pessoa.nome)