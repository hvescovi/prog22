from ac_classe_pessoa import *

class Exame():
    def __init__(self, data, descricao, resultado, p):
        self.data = data
        self.descricao = descricao
        self.resultado = resultado
        self.pessoa = p

    def __str__(self):
        return f'Data: {self.data}, exame: {self.descricao}, resultado: '+\
               f'{self.resultado}, quem fez: {str(self.pessoa)}'

# cria uma pessoa
p1 = Pessoa(nome = "João da Silva", email = "josilva@gmail.com",
            telefone = "47 99012 3232")

# cria um exame de João
e1 = Exame("12/01/2020", "Vitamina B12", "219 pg/mL", p1)

# exibe os dados
print(e1)