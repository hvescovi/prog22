class Pessoa:
    def __init__(self, nome, email, telefone):
        self.nome = nome
        self.email = email
        self.telefone = telefone

    # método para expressar a pessoa em forma de texto
    # usando concatenação de strings
    def __str__(self):
        return self.nome + ", " + self.email + ", " + self.telefone

# teste da classe
alguem = Pessoa("João da Silva","josilva@gmail.com", "47 99012 3232")
            
# exibir a pessoa
print(alguem.nome, alguem.email, alguem.telefone)