from datetime import date

class Pessoa:
    def __init__(self, nome="", email="", telefone=""):
        self.nome = nome
        self.email = email
        self.telefone = telefone

    def __str__(self):
        return f'{self.nome}, '+\
               f'{self.email}, {self.telefone}'

class Vendedor(Pessoa):
    def __init__(self, nome="", email="", telefone="", comissao=0.0):
        # invocando o construtor do pai
        super().__init__(nome=nome, email=email, telefone=telefone)
        self.comissao = comissao

    def __str__(self):
        return f'{super().__str__()}, comissão do vendedor = {self.comissao}'

class Motorista(Pessoa):
    def __init__(self, nome="", email="", telefone="", cnh = ""):
        super().__init__(nome=nome, email=email, telefone=telefone)
        self.cnh = cnh
    def __str__(self):
        return f'{super().__str__()}, '+\
            f'carteira nacional de habilitação do motorista = {self.cnh}'

# cria um vendedor que ganha 10% de comissão
v = Vendedor(nome = "João da Silva", email = "josilva@gmail.com",
            telefone = "47 99012 3232", comissao = 0.1)

# cria um motorista
m = Motorista(nome = "Maria de Oliveira", email = "maliv@gmail.com", 
            telefone = "47 91212-2323", cnh = "15243-6")

# insere essas pessoas em uma lista
lista = []
lista.append(v)
lista.append(m)
# adiciona uma data e um texto pra bagunçar a lista :-)
lista.append(date(2022, 11, 13)) # 13/11/2022
lista.append("Olá mundo")

# mostra o que estiver na lista
for p in lista:
    # mostra!!
    print(p)

    # o elemento atual da lista é um motorista?
    if isinstance(p, Motorista):
        print('eu mostrei um(a) motorista')
    # o elemento atual da lista é um vendedor?
    elif isinstance(p, Vendedor):
        print('foi exibido(a) um(a) vendedor')
    else:
        print('mostrei algo que não era nem motorista nem vendedor')
        
    # o elemento da atual da lista que foi mostrado era uma pessoa?
    if isinstance(p, Pessoa):
        print('===> NOSSA, EU MOSTREI UMA PESSOA, que tem nome :-) ===> '+p.nome)

    # alguma data foi exibida?
    if type(p) == date:
        print('===> caramba, eu mostrei uma DATA! :-) maneiro')
    # isinstance também funciona para tipos primitivos, pois tudo é objeto em python
    if isinstance(p, date):
        print('===> data, data, data é objeto; tudo em python é objeto, eita!')        
    
    # mostra um divisor de item de lista
    print("-------------------------------------------")
    # type => vai bem para tipos primitivos
    # isinstance => melhor para qualquer tipo utilizado (primitivos ou criados)