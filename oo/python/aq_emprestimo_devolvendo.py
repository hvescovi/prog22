class Livro:
    def __init__(self, nome="", ISBN=""):
        self.nome = nome
        self.isbn = ISBN
    def __str__(self):
        return f'{self.nome}, {self.isbn}'

class ExemplarDeLivro:
    def __init__(self, livro, codigo):
        self.livro = livro
        self.codigo = codigo
    def __str__(self):
        return f'{self.livro}, {self.codigo}'

# teste
livro1 = Livro("Python 3", "123321123")
ex1 = ExemplarDeLivro(livro1, "AX123")
print("exemplar de livro:", ex1)

class Pessoa:
    def __init__(self, nome="", email="", telefone=""):
        self.nome = nome
        self.email = email
        self.telefone = telefone
    def __str__(self):
        return f'{self.nome}, '+\
               f'{self.email}, {self.telefone}'

class Aluno(Pessoa):
    def __init__(self, nome="", email="", tel="", matricula=""):
        super().__init__(nome=nome, email=email, telefone=tel)
        self.matricula = matricula
    def __str__(self):
        return f'{super().__str__()}, {self.matricula}'

# teste do aluno
aluno1 = Aluno(nome="Joaozinho", email="josilva@gmail.com", tel = "91122-3344", matricula = "2022010203")
print("aluno:", aluno1)

class Servidor(Pessoa):
    def __init__(self, nome="", email="", tel="", siape=""):
        super().__init__(nome=nome, email=email, telefone=tel)
        self.siape = siape
    def __str__(self):
        return f'{super().__str__()}, {self.siape}'

serv1 = Servidor(nome="John Them", email="jothen@gmail.com", tel="9 1221-2332", siape="1928374")
print("servidor:", serv1)

class Multa:
    def __init__(self, emprestimo=None, valor=0.0, paga=False):
        self.emprestimo = emprestimo
        self.valor = valor
        self.paga = paga
    def __str__(self):
        return f'{self.emprestimo}, {self.valor}, {self.paga}'
    
class Emprestimo():
    def __init__(self, emprestado=None, devolvido=None, pessoa=None, exemplar=None):
        self.emprestado = emprestado
        self.devolvido = devolvido
        self.pessoa = pessoa
        self.exemplar = exemplar
    def __str__(self):
        return f'{self.emprestado:%d/%m/%Y}, {self.devolvido}, {self.pessoa}, {self.exemplar}'

    def devolver(self, data_devolucao=None):
        '''
        retorna None se não for gerada multa
        caso contrário,o retorna a multa gerada
        '''

        # marca que o livro foi devolvido
        self.devolvido = data_devolucao

        if isinstance(self.pessoa, Aluno): # quem pegou o livro foi um aluno?
            prazo = 15 # prazo de 15 dias para aluno
        else:
            prazo = 30 # assumir que é servidor: prazo de 30 dias
        
        # qual é a diferença de dias entre a data do empréstimo e a data da devolução?
        dias = abs(self.devolvido - self.emprestado).days
        print(dias, "prazo", prazo)

        # a data de devolução está dentro do prazo de entrega?
        if dias <= prazo:
            # tudo certo, não precisa de multa
            return None
        else:
            # vai gerar multa (dois reais por dia)
            m1 = Multa(self, dias*2, False)
            return m1
        
from datetime import *

# teste do emprestimo
emp1 = Emprestimo(emprestado = datetime.today(), pessoa = aluno1, exemplar = ex1)
print("Empréstimo:", emp1)

# teste de emprestimo com multa
emp2 = Emprestimo(emprestado = date(2022, 10, 1), pessoa = aluno1, exemplar = ex1)
print("Outro empréstimo (vai gerar multa):", emp2)
kkk = emp2.devolver(date(2022, 11, 1))
print("Multa:", kkk)