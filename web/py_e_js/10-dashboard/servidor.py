# importações
from flask import Flask, request, Response

# configurações
app = Flask(__name__)

@app.route("/")
def inicio():
    partes = request.host.split(":")
    resposta = Response('Note do HYLSON ==> '+\
        '<a href="http://'+partes[0]+':8080">dashboard (front-end)</a>')
    resposta.headers.add("Access-Control-Allow-Origin", "*")        
    return resposta

app.run(debug=True, host='0.0.0.0')
