$(function () {

  function atualiza() {
    // lista de sites a ser percorrida
    var texto = $("#enderecos").val();
    var sites = texto.split("\n");

    //alert(sites);
    $("#consultando").empty();
    $("#resultado").empty();
    $("#comerro").empty();
    for (let s of sites) {
      $("#consultando").append("consultando: " + s + " | ");
      // nova imagem
      var img = $('<img id="imagem' + s + '" width="15">');
      img.attr('src', 'carregando.gif');
      img.appendTo('#consultando');
      $.ajax({
        url: s,
        success: function (resposta) {
          $("#resultado").append("<br> "+s+" ===> " + resposta);
          $(document.getElementById("imagem"+s)).remove();
        },
        error: function () {
          $("#comerro").append('| erro ao acessar: ' + s);
          $(document.getElementById("imagem"+s)).remove();
        },
        timeout: 10000
      });
    }
  }

  $(document).on("click", "#btVai", function () {
    atualiza();
  }
  );

  atualiza();

});