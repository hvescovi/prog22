FLASK em modo de produção com waitress
---

1) instalar:
pip3 install waitress

2) importar:
from waitress import serve

3) substituir:

app.run(host='0.0.0.0')

por:

serve(app, host='0.0.0.0', port=5000)

Referência:
https://dev.to/thetrebelcc/how-to-run-a-flask-app-over-https-using-waitress-and-nginx-2020-235c

* deploy com logs: além do import anterior:
1) pip3 install paste
2) from paste.translogger import TransLogger
import logging.config
logging.basicConfig(
    filename='meuslogs.log', 
    level=logging.INFO
)
3) substituir app.run anterior por:
serve(TransLogger(app, setup_console_handler=False), host='0.0.0.0', port=5000)

Formato do arquivo de log:
https://httpd.apache.org/docs/2.4/logs.html