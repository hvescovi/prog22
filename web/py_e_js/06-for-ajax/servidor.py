from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def ola():
    return "<b>Olá, mundo!</b>"

@app.route("/rota1")
def rota1():
    resposta = jsonify({"mensagem":"esta é a primeira rota"})
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta

@app.route("/rota2")
def rota2():
    resposta = jsonify({"mensagem":"rota dois na área"})
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta

@app.route("/rota3")
def rota3():
    resposta = jsonify({"mensagem":"opa temos uma terceira rota"})
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta

app.run(debug=True)
