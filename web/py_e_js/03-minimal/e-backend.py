from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
import os

path = os.path.dirname(os.path.abspath(__file__)) 
arquivobd = os.path.join(path, 'pessoa.db')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Pessoa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    email = db.Column(db.String(254))
    telefone = db.Column(db.String(254))

    def __str__(self):
        return self.nome + "[id="+str(self.id)+ "], " +\
            self.email + ", " + self.telefone

    def json(self):
        return { "id": self.id, 
                 "nome": self.nome, 
                 "email": self.email, 
                 "telefone": self.telefone
               }

@app.route("/")
def ola():
    return "Servidor backend operante"

@app.route("/listar")
def listar():
    pessoas = db.session.query(Pessoa).all()
    lista = []
    for p in pessoas:
        lista.append(p.json())
    resposta = jsonify(lista)
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta

app.run(debug=True)

# teste: curl localhost:5000/listar