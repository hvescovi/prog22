from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

path = os.path.dirname(os.path.abspath(__file__)) 
arquivobd = os.path.join(path, 'pessoa.db')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

app.app_context().push() # comando necessário para python versão > 10

class Pessoa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    email = db.Column(db.String(254))
    telefone = db.Column(db.String(254))

    def __str__(self):
        return self.nome + "[id="+str(self.id)+ "], " +\
            self.email + ", " + self.telefone

db.create_all()

p1 = Pessoa(nome = "João da Silva", email = "josilva@gmail.com", 
telefone = "47 99012 3232")
db.session.add(p1)
db.session.commit()

print(p1)