from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def ola():
    return "Servidor backend operante"

@app.route("/obter_dados")
def criar_tabelas():
    dados = {
        "x": "['giraffes', 'orangutans', 'monkeys']",
        "y": "[20, 14, 23]",
        "type": "bar"
    }
    retorno = {"resultado":"ok"}
    retorno.update({"detalhes":dados});
    resposta = jsonify(retorno)
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta

app.run(debug=True)